// Initializes the `weather` service on path `/weather`
const createService = require('feathers-nedb');
const createModel = require('../../models/weather.model');
const hooks = require('./weather.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    // paginate
  };

  // Initialize our service with any options it requires
  app.use('/weather', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('weather');

  service.hooks(hooks);
};
